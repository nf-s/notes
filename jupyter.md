
# Jupyter Notebook Tips

## Anaconda headless
`export PATH="$HOME/anaconda3/bin:$PATH"  `
OR  
Run in each bash session - `source $HOME/anaconda3/bin/activate`

## Installation on AWS
1. cd ~
2. sudo /home/ubuntu/src/anaconda3/bin/pip install keras --upgrade
3. jupyter notebook --generate-config
4. jupyter notebook password

5. Edit ~/.jupyter/jupyter_notebook_config.py  
   + (https://jupyter-notebook.readthedocs.io/en/latest/public_server.html)  

        ```
        # Set options for certfile, ip, password, and toggle off
        # browser auto-opening
        c.NotebookApp.certfile = u'/absolute/path/to/your/certificate/mycert.pem'
        c.NotebookApp.keyfile = u'/absolute/path/to/your/certificate/mykey.key'
        # Set ip to '*' to bind on all interfaces (ips) for the public server
        c.NotebookApp.ip = '*'
        c.NotebookApp.open_browser = False

        # It is a good idea to set a known, fixed port for server access
        c.NotebookApp.port = 9999
        c.NotebookApp.notebook_dir = '/home/ubuntu'
        ```

6. src/anaconda3/bin/jupyter notebook
1. git clone https://github.com/bstriner/keras_adversarial.git
1. cd keras_adversarial
1. python setup.py install
7. ../src/anaconda3/bin/python setup.py install

### List available Jupyter notebooks to find token

In Anaconda prompt - `jupyter notebook list`

## Jupyter tips

### Keyboard shortcuts [In Command Mode]

Command Palette - `<P>`  
Toggle Cell Output - `<O>`  
Insert cell below - `<B></B>`  
Insert cell above - `<A>`  
Run cell - `<Ctl>+<Enter>`  
Run cell + Select cell below - `<Shift>+<Enter>`  
Run cell + Create cell below - `<Alt>+<Enter>`  
Cell type = markdown - `<M>`  
Cell type = code - `<Y>`  

### Keyboard shortcuts [In Edit Mode]

*Show Keyboard shortcuts - `<H>`*  
Show tooltip - `<Shift>+<Tab>`  
Code completion - `<Tab>`  
Command mode - `<Escape>`  
Split cell - `<Ctrl>+<Shift>+<->`

## Jupyter *lab* tips
shows tab bar - `ctl+shift+enter`