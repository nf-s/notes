# Markdown tips

https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet


https://daringfireball.net/projects/markdown/basics   

## Markdown syntax

H1 Header - `# Header` or `=========`  
H2 Header (or Line separator) - `## Header 2` or `---------------`  
H3 Header - `### Header 3`  
H4 Header - `#### Header 3`  

Line break - two or more spaces at end of line  
Paragraphs - Blank line between text  

Code - backtick ``(`)``  
Code Block - triple backtick ``(```)``  -  or indented with four spaces.  
Block quote - `>`  

Emphasis - asterisks (*) and underscores (_) 

+ *single=*`<em>`
+ **double=**`<strong>`  

Strike through - `~~ Text ~~`  

Hyperlink - `[Name](Href)`

### HTML needed for...
Emdash - `&mdash;` &mdash;

{super,sub}script - `<sup>...</sup>`, `<sub>...</sub>`

## VS Code keybindings

```json
[
    {
        "key": "ctrl+alt+x",
        "command": "editor.action.insertSnippet",
        "args": {
            "snippet": "```math\n$0\n```"
        },
        "when": "editorTextFocus && editorLangId == markdown"
    },{
        "key": "ctrl+alt+z",
        "command": "editor.action.insertSnippet",
        "args": {
            "snippet": "$`$0`$"
        },
        "when": "editorTextFocus && editorLangId == markdown"
    }
]
```