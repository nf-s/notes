# Latex Tips

https://wch.github.io/latexsheet/latexsheet-a4.pdf

In Markdown - https://katex.org/docs/support_table.html


## Maths

https://www.sharelatex.com/learn/Mathematical_expressions  

https://en.wikipedia.org/wiki/Wikipedia:LaTeX_symbols

`$...$` for inline mathematics and `$$...$$` for displayed mathematics.

`$$ x^n + y^n = z^n $$`

$$ x^n + y^n = z^n $$
  
`$ x^n + y^n = z^n $` - 
$x^n + y^n = z^n$ 

$$
T_w = T \atan{0.151 977(RH  18.313 659)^(1/2)} \atan{T RH } 2 \atan{RH 21:676 331} 10.003 918 38 (RH)^(3/2) \atan{0.023 101 RH} 24.686 035
$$


### Examples

`$ loss = (r + \gamma{}.max\hat{Q}(x, a') - Q(s,a) $`  

$loss = (r + \gamma{}.max\hat{Q}(x, a') - Q(s,a)$

## Misc

https://en.wikipedia.org/wiki/List_of_mathematical_symbols_by_subject

Fractions - `$\frac{1}{2}$` - $\frac{1}{2}$

Curley Brackes {} - `\{`

Space - `\,`

Double integral - `\iint_R` - $\iint_R$

$\in$ - `\in`

$\lim_{x\to\infty} - `$\lim_{x\to\infty }`

$\mathbb{R}$ - `\mathbb{R}`

$\sim$ - `\sim`

$\rightsquigarrow$ - `\rightsquigarrow`

$`\propto`$ - `\propto`

### Piecewise functions

```latex
\begin{cases} 
      0 & x\leq 0 \\
      \frac{100-x}{100} & 0\leq x\leq 100 \\
      0 & 100\leq x 
   \end{cases}
```

$$
\begin{cases} 
      0 & x\leq 0 \\
      \frac{100-x}{100} & 0\leq x\leq 100 \\
      0 & 100\leq x 
   \end{cases}
   $$

### Multiple line equations

Using `\begin{aligned}` and `\end{aligned}`.  
Then use `\\` for line breaks and `&` for align points.

Example:

$$\begin{aligned}F(x_1, x_2, ..., x_k)&=  \int^{x_1}_{-\infty}\int^{x_2}_{-\infty}...\int^{x_k}_{-\infty} F(t_1, t_2, ...,t_k) dt_1dt_2...dt_k \\

 \forall\underline{x}&=(x_1, x_2, ..., x_k)
\end{aligned}$$

### Evaluated at - vertical bar:

$$
\frac{d^n M_{X-\mu}(t)}{dt^n}\Bigg|_{t=0}
$$

## Spaces

https://tex.stackexchange.com/questions/74353/what-commands-are-there-for-horizontal-spacing

`\quad`

## Matrices

```math
\begin{bmatrix}
    x_{11}       & x_{12} & x_{13} & \dots & x_{1n} \\
    x_{21}       & x_{22} & x_{23} & \dots & x_{2n} \\
    \vdots \\
    x_{d1}       & x_{d2} & x_{d3} & \dots & x_{dn}
\end{bmatrix}
=
\begin{bmatrix}
    x_{11} & x_{12} & x_{13} & \dots  & x_{1n} \\
    x_{21} & x_{22} & x_{23} & \dots  & x_{2n} \\
    \vdots & \vdots & \vdots & \ddots & \vdots \\
    x_{d1} & x_{d2} & x_{d3} & \dots  & x_{dn}
\end{bmatrix}
```