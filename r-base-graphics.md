
# Plotting functions

## Setting graphical parameters


```R
install.packages("RColorBrewer")
library(RColorBrewer)

display.brewer.all()
colorRampPalette(brewer.pal(9,"YlOrRd"))(100)
```


```R
x = par(...)
```

## Colour Brewer
https://www.r-bloggers.com/r-using-rcolorbrewer-to-colour-your-figures-in-r/

Params:
+ `"mfrow"` = multiple plots on screen
    + `c(2,2)` = 2x2 grid
+ `new=TRUE` - plot over existing (overlay plots)
+ `"mai"` = c(bottom, left, top, right) - Margin in inches
+ `fig` = A numerical vector of the form c(x1, x2, y1, y2) which gives the (NDC) coordinates 

** Must then close with**


```R
d[id_points,]
```


```R
par(x)
-or-
par(mfrow=c(1,1))
```

Click on points on plot, then can show highlighted points with:

### Complex plot layout
http://www.statmethods.net/advgraphs/layout.html  
Good formarginal density plot


```R
plot(Y ~ X,data=d)
id_points <- identify(d$X, d$Y)
```

![marginal density plot](http://www.sthda.com/sthda/RDoc/figure/ggplot2/ggplot2-mixing-multiple-plots-ggextra-marginal-plots-1.png)

## Identify points on plot

Plot | Matrix |  |  |
--- | --- | --- | ---
Plot 3 | Plot 3 | Plot 3 | Plot 2
Plot 3 | Plot 3 | Plot 3 | Plot 2
Plot 3 | Plot 3 | Plot 3 | Plot 2
Plot 1 | Plot 1 | Plot 1 | ------


```R
library(maps)
map("usa",xlim=c(-125,-70),ylim=c(25,49),add=T)
```


```R
layout( matrix(c(3,3,3,1,
                 3,3,3,1,
                 3,3,3,1,
                 2,2,2,0),ncol=4) ) 
```

Params:
+ `col=`
   + heat.colors, terrain.colors and topo.colors
   
## Plot map
Example:

Or 

Plot | Matrix 
--- | --- 
1 | 2 
3 | 3


```R
image(x, y, z,col=heat.colors(64))
```


```R
layout(matrix(c(1,2,3,3), 2, 2, byrow = TRUE))
```

## Image plot

### Plot funciton


```R
plot.design(Time ~ Treat * Poison, data = dd, fun = mean, main="Means")
plot.design(Time ~ Treat * Poison, data = dd, fun = median, main="Medians")
```


```R
plot(Y~X, data=d,...)  
plot(data$Y, data$X) 
```

#### Params:
+ Types
   + "p" plots the points
   + "r" plots the regression lines
   
## Design Plot
> Plot Univariate Effects of a Design or Model

http://www.statmethods.net/advgraphs/parameters.html  
### Plot Params:
+ `"type"`
    + p=points
    + l=lines
    + b=both
    + n=none


+ `"main"` = title
+ `"sub"` = subtitle
+ `"xlab"`, `"ylab"`...
+ `"cex"` =  magnification
+ `"which"` = plot to show (if multiple plots - i.e. plot(lm(...)))
+ `"lwd"` =  line width
+ `"lend"` =  line end cap
   + 0 = and "round" mean rounded line caps [default];
   + 1 = and "butt" mean butt line caps;
   + 2 = and "square" mean square line caps.
+ `"xaxt" or "yaxt"='n'` = hide x or y axis
+ `"bty"="n"` - hide border (box border) 
    + `frame.plot=FALSE`
+ `zero.line=FALSE` - remove base line (y=0 line)

Plot symbols:
+ `"pch"=20` - bullet point


```R
xyplot(Weight ~ Age | Sex*Season,data=d,type=c("p","r"))
```

### Add Title to plot

Example:


```R
title(year, line = -1)
```


```R
xyplot(Y~X | G1 * G2)
```

+ line = position
+ adj =  0 [left-justified text], 0.5 [centered text] and 1 [right-justified text]
    + or c(x, y)

### Add Axis to plot

## Bivariate Scatter Plot
`library(lattice)`  
https://www.stat.ubc.ca/~jenny/STAT545A/block09_xyplotLattice.html

For separate graph for each combination of G1 and G2


```R
axis(location, at=x_values, labels=x_labels, tick = [FALSE])
# Nice small ticks
axis(..., lwd=0, tck=-0.02, lwd.ticks=1)
```


```R
boxplot (x, ...)
```

+ `location` 1=below, 2=left, 3=above and 4=right.

### Add text to plot

## Boxplot


```R
text(x,y, labels)
```


```R
hist(obj, breaks, ...)
```

### Write text to margins of plot

## Histogram


```R
mtext(text, side = 3)
```


```R
pairs(obj)
```

+ `side` =  1=bottom, 2=left, 3=top, 4=right

## Scatter plot matrix

### Add straight line to plot


```R
barplot(x, BESIDE=, LEGEND="topright")
```


```R
abline(lm(...), col='blue')
```

## Barplot

### Add connected line segments - good for curves


```R
par(xpd=TRUE)
legend(...)
par(xpd=FALSE)
```


```R
lines(x, y)
```

+ `xy` = "topright"...
+ `title = `,  
+ `border=FALSE`, 
+ `box.col=FALSE`

To print outside of plot:  

### Legend


```R
legend(xy, titles, "fill"=c(c1, c2...))
```
