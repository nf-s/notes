# Stardust.js

## Mark Spec

### Data Types

+ `Vector2`, `Vector3`...
    + Fields accessed by .x, .y, .z, .w  
    `evaluator/evaluator.js` Line 35
        ```javascript
            case "x": return value[0];
                    case "y": return value[1];
                    case "z": return value[2];
                    case "w": return value[3];
        ```
### Functions

`instrinsics` folder

+ mix()
+ clamp()... -  Error"no matching overloaded function found"