# Designing Data-intensive Applications

## Chapter 1 - Reliability, Scalability and Maintanability

### Reliability

System should work correctly when faults occur - hardware (typically random and uncorrelated), software (bugs are typically systemic and hard to deal with), humans (make mistakes)

Sandboxed environment for demo users

### Scalability

Stable performance with load increase

#### Response time percentiles

Look at request response times (in percentiles not averages) - especially for backend services (as a front-end response time may be limited by the maximum response time for a back-end request)

Can use forward decay, t-digest or HdrHistrogram to give good approximiations

> An architecure that scales well for a particular application is built around assumptions of which operations will be common and which will be rare &mdash; the load parameters

### Maintanability

#### Operability - Make easy for operations team to keep system running smoothly

> Good operations can work around imitations of bad (or incomplete) software, but good software can't run reliably with bad operations

- Monitor health, quickly restore service if it goes into a bad state
- Tracking down cause of problems (system failures, bad performance)
- Updating (security patches ...)
- Monitoring how different systems interact - to prevent problematic changes
- Anticipating future problems
- Establishing good practice and tools for deployment, config...
- Performing complex maintenance tasks, such as moving from one platform to another
- Maintaining security as confiuration chages
- Define processes that make operations predictable and keep the environment stable
- Preserving the organisation's knowledge about the system (as people come and go)

##### Good operability in data systems

- Visiblity into runtime behavious and internals of system
- Support for automation and integration with standard tools
- Avoid dependency on individual machines (allow rolling updates etc)
- Good documentation and an easy to understand operational model (eg If I do $X$, $Y$ will happen)
- Good default behaviour - but allow overriding defaults
- Self-healing where appropriate - but allow manual control when needed
- Make predictable - minimise surprises

#### Splicity - Easy for new engineers to understand system, by managing complexity

##### Possible symptoms

- Explosion of state space
- Tight coupling of modules
- Tangled dependencies
- Inconsistent naming and terminology
- Hacks aimed at solving performance problems
- Special casing to work around issues elsewhere

_See "No Silver Bullet - Essence and Accident in Software Engineering" in The Mythical Man Month_

Complexity $\implies$ hidden assumptions, unintended consequences and unexpected interactions are more overlooked

Accidental complexity:

> Complexity which is not inherent in the problem, but a matter of implementation

Can solve accidental complexity using **Abstraction**

> A good abstraciton can hide a great deal of implementaation detail behind a clean, easy-to-understand facade

#### Evolvability - Easy to adapt and further develop

Test-driven development, refactoring...

Closely linked with simplicity and its abstractions - simple and easy-to-understand = easier to modify

# Chapter 2 - Data Models and Query Languages

## Data Models

### Many-to-one and Many-to-many relationships

_Normalisation_ to reduce duplication.

Relational DB are more for this - but MongoDB (and others) support _join_ queries with some overhead.

### The network model

The hierarchical model (i.e. JSON) only allows one parent for each record (node), the network model allows records to have multiple parents.

Links between records stored _like_ pointers &mdash; the only way to access a record was to follow a path from a root record along these chain links. Called an **access path**.

Very difficult to change data model

### The document (hierarchical) model

More flexible than relational, and more performant due to **locality** (database model is more similar to Applications data model).

Worse at representing many-to-one/many.

#### Which model leads to simpler code?

If application data is document-like, preferrably use document model DB.

Relational _shredding_ (splitting structure into multiple tables) can lead to cumbersome schema and complicated code.

But with document-model it can be tricky to refer to nested items.

For highly connected data - **Graph-like data models** may be better.

#### Flexible schema

Document DBs have _implicit_ schema or schema-on-read &mdash; that is not enforced by DB.

Easier to update in the future, can handle more heterogeneous data, more variation in models...

#### Locality

Good if it reduces multiple lookups (across multiple tables), but bad if only small chunks of objects are needed at any given time (the DB usually needs to load whole documents at once, which can be wasteful).

Similarly, documents usually are completely re-written on update (if the update changes the encoded size of the document), whereas relational DBs allow partial updating (per row in a given table).

Therefore try to keep documents small, and avoid writes which increase size of document.

_Note:_ some relational models have ways of grouping related data for locality (eg _Column-family_ in Bigtable data model - used in Cassandra and HBase)

#### Convergence of document and relational DBs

JSON support.

Joins in Document model...

## Query Languges

**Imperative** = perform certain ops in certain order

**Declarative** = specify goal, but not how to achieve it (eg SQL - pattern of data, conditions, transformations...)

- usually more concise and easier
- hides implementation detail of DB engine
- lends well to parallelisation - DB can implement queries in any way
