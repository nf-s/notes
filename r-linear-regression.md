
# Regression functions

## Linear Regression


```R
lm(Y~X), data=d)
```

#### Interaction terms


```R
X1:X2

# Note
X1*X2 => X1+X2+X1:X2
```

#### Add powers to lm


```R
I(X^2)
```

+ `I()` = "as is"
+ Indices in R default to vector multiplication

#### Remove intercept
Add -1 in model


```R
lm(Y~X-1, ...)
```

#### Term addition + Deletion
> Essentially manual stepwise

Example:  


```R
lm1 <- lm(Y ~ X1, data = d)
add1(lm1,. ~ . X2, test = "F")
drop1(lm1, .~ X2, test = "F")  # So called 'type II' anova
```

Params:
+ `test=F` - F-test to compare original model to model with term addition

### Summary of linear model


```R
summary(lm(...))
```

+ Adjusted r-sq = variability explained by model = accuracy of model prediction
   + "Adjusted" for degrees of freedom
   
   
+ `coef(fit)`
+ `residuals(fit)`
+ `fitted(fit)`
   
### 95% Confidence Intervals for Coefficients


```R
confint(fit)
```

### Confidence Regions
When you have multiple varibales, interpretting the confidence intervals seperately is not appropriate, you need to look at the shared confidence region for the two (or more) variables


```R
conf.ellipse <- function(fit,which=c(1,2),level=0.95, npoints=100) {
  r <- sqrt(2*qf(level,2,fit$df.residual))
  a <- seq(0, 2*pi, len = npoints)
  rep(coef(fit)[which],each=npoints)+
    cbind(r*cos(a),r*sin(a))%*%chol(vcov(fit)[which,which])
}

## Confidence regions vs intervals for third and fourth coefs
bs <- 3:4
plot(conf.ellipse(fit,bs),type="l")
points(coef(fit)[bs[1]],coef(fit)[bs[2]])
abline(v=confint(fit,bs[1]))
```

   
### Diagnostic plots
Residuals vs Fitted
+ Look for homoscedasticity - equal variance of residuals for each fitted value

Normal Q-Q
+ See if assumption of normally distributed residuals is met

Residual vs Leverage
+ Values beyond 2 std. error

### Influence measures


```R
summary(influence.measures(fit))
```

+ `*` indicates significance
+ ignore covariance

#### Row Meanings:
+ dfBeta - how much our regresion coefficients change if the point is removed
+ dffit - how much the fitted value changes if the point is removed
+ cook.d and hat are both measures of influence (higher = more influential)

## ANOVA
Term order matters  
Tests for overall fit of model


```R
anova(lm(Y~X, ...)) 
aov(Y~X, ...)
```

### Predict using model


```R
predict(fit,data.frame(X=seq(0,x.max,x.step)))
```

## Tests for homogeneity of variance

#### Bartlett Test


```R
bartlett.test(Y ~ X,data=d)
```

#### Levene's Test


```R
library(car)
leveneTest(d$Y,d$X)
```

## Model Selection/Transformation
### Box Cox Transformation
http://www.isixsigma.com/tools-templates/normality/making-data-normal-using-box-cox-power-transformation/  
Indicates possible transformation on response variable (y)


```R
library(mass)
boxcox(Y~X, data=d)
```

$\lambda$ values:
+ $\lambda = 1$ - no transformation
+ $\lambda = 0$ - ln(Y)
+ other - $Y^\lambda$

### Stepwise


```R
sfit <- step(fit,scope=list(
                   lower= . ~ 1,
                   upper= . ~ X1+X2+I(X1^2)+I(X2^2)+I(X1*X2)+...))
```

Can force lower limit:  


```R
sfit <- step(fit,scope=list(
                   lower= . ~ X1+X2,
                   upper= . ~ X1+X2+I(X1^2)+I(X2^2)+I(X1*X2)+...))
```

## Post-hoc tests
### Tukey's HSD


```R
TukeyHSD(aov(Y~X...), which = "")
```

+ `which` = vector for which the intervals should be calculated.

### General linear hypotheses and multiple comparisons for parametric models


```R
library(multcomp)
mc <- glht(aov(Y~X...),linfct=mcp(Xi="Tukey"))
summary(mc)
plot(mc)
```

+ `linfct` = linear hypotheses to be tested
   + `mcp` = for parameters of interest in two-way ANOVA or ANCOVA models
      + `Xi` = vector for comparison
      + `"Tukey"` = method

### Multiplicity adjusted t-tests assuming unequal group variances


```R
pairwise.t.test(d$Y ,d$X,pool.sd=FALSE)
```

+ `p.adj` = what type of multiple comparison
   + Default = "holm"
   

### Games Howell test - custom fuction
From - https://rpubs.com/aaronsc32/games-howell-test  
The Games-Howell post-hoc test is another nonparametric approach to compare combinations of groups or treatments. Although rather similar to Tukey’s test in its formulation, the Games-Howell test does not assume equal variances and sample sizes. The test was designed based on Welch’s degrees of freedom correction and uses Tukey’s studentized range distribution, denoted qq. The Games-Howell test is performed on the ranked variables similar to other nonparametric tests. Since the Games-Howell test does not rely on equal variances and sample sizes, it is often recommended over other approaches such as Tukey’s test.


```R
## So here is the code that the super awesome guy wrote 
## that will get our games howell confidence intervals.

## Just highlight the whole thing and read it all in.

## Then, once you've done that, you can try your analysis...
## something like (assuming you called your data "rind")

## games.howell(rind$SiteNum, rind$Thickness)

## That should get you what you want.

games.howell <- function(grp, obs) {
  
  #Create combinations
  combs <- combn(unique(grp), 2)
  
  # Statistics that will be used throughout the calculations:
  # n = sample size of each group
  # groups = number of groups in data
  # Mean = means of each group sample
  # std = variance of each group sample
  n <- tapply(obs, grp, length)
  groups <- length(tapply(obs, grp, length))
  Mean <- tapply(obs, grp, mean)
  std <- tapply(obs, grp, var)
  
  statistics <- lapply(1:ncol(combs), function(x) {
    
    mean.diff <- Mean[combs[2,x]] - Mean[combs[1,x]]
    
    #t-values
    t <- abs(Mean[combs[1,x]] - Mean[combs[2,x]]) / sqrt((std[combs[1,x]] / n[combs[1,x]]) + (std[combs[2,x]] / n[combs[2,x]]))
    
    # Degrees of Freedom
    df <- (std[combs[1,x]] / n[combs[1,x]] + std[combs[2,x]] / n[combs[2,x]])^2 / # Numerator Degrees of Freedom
      ((std[combs[1,x]] / n[combs[1,x]])^2 / (n[combs[1,x]] - 1) + # Part 1 of Denominator Degrees of Freedom 
         (std[combs[2,x]] / n[combs[2,x]])^2 / (n[combs[2,x]] - 1)) # Part 2 of Denominator Degrees of Freedom
    
    #p-values
    p <- ptukey(t * sqrt(2), groups, df, lower.tail = FALSE)
    
    # Sigma standard error
    se <- sqrt(0.5 * (std[combs[1,x]] / n[combs[1,x]] + std[combs[2,x]] / n[combs[2,x]]))
    
    # Upper Confidence Limit
    upper.conf <- lapply(1:ncol(combs), function(x) {
      mean.diff + qtukey(p = 0.95, nmeans = groups, df = df) * se
    })[[1]]
    
    # Lower Confidence Limit
    lower.conf <- lapply(1:ncol(combs), function(x) {
      mean.diff - qtukey(p = 0.95, nmeans = groups, df = df) * se
    })[[1]]
    
    # Group Combinations
    grp.comb <- paste(combs[1,x], ':', combs[2,x])
    
    # Collect all statistics into list
    stats <- list(grp.comb, mean.diff, se, t, df, p, upper.conf, lower.conf)
  })
  
  # Unlist statistics collected earlier
  stats.unlisted <- lapply(statistics, function(x) {
    unlist(x)
  })
  
  # Create dataframe from flattened list
  results <- data.frame(matrix(unlist(stats.unlisted), nrow = length(stats.unlisted), byrow=TRUE))
  
  # Select columns set as factors that should be numeric and change with as.numeric
  results[c(2, 3:ncol(results))] <- round(as.numeric(as.matrix(results[c(2, 3:ncol(results))])), digits = 3)
  
  # Rename data frame columns
  colnames(results) <- c('groups', 'Mean Difference', 'Standard Error', 't', 'df', 'p', 'upper limit', 'lower limit')
  
  return(results)
}
```

## Welch's ANOVA
Does not assume homogeneity of variance


```R
oneway.test(Y ~ X,data=d,var.equal=FALSE)
```

## Non-parametric - rank sum tests
### Kruskal Wallis rank sum test


```R
kruskal.test(Y ~ X,data=d)
```

### Multiplicity adjusted Wilcoxon rank sum tests


```R
pairwise.wilcox.test(d$Y,d$X)
```

## Todo
+ Bootstraping
+ Contrasts
---
