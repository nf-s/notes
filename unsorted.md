# Unsorted

## Bash

Count files in dir: `ls -1q some-dir/ | wc -l`

Count number o lines in file: `wc -l file`

Remote filetransfer with rsync: `rsync -aHAXxv --numeric-ids --delete --progress -e "ssh -T -c arcfour -o Compression=no -x -i .ssh/nick-gcp.key" /home/for321/Documents/geoserver-2.15.1/data_dir.zip nick@climatemap.duckdns.org:/home/nick/geoserver-2.15.1/data_dir2.zip`

### Dealing with large CSVs

Print first 10 lines: `head -10 bar.txt`

Sort column 1 (-n = numeric): `sort -t',' -n -k1 filename`

Get unique values in column 1 (must be sorted): `cat abc.csv | cut -f1 -d , | uniq`

## JS

### RGB to HEX

// from https://stackoverflow.com/a/5624139

```js
function componentToHex(c) {
  var hex = c.toString(16);
  return hex.length == 1 ? "0" + hex : hex;
}
```

## Geoserver

### D3 colourscheme to SLD (XML)

```js
// Magma from d3.js
specifier =
  "00000401000501010601010802010902020b02020d03030f03031204041405041606051806051a07061c08071e0907200a08220b09240c09260d0a290e0b2b100b2d110c2f120d31130d34140e36150e38160f3b180f3d19103f1a10421c10441d11471e114920114b21114e22115024125325125527125829115a2a115c2c115f2d11612f116331116533106734106936106b38106c390f6e3b0f703d0f713f0f72400f74420f75440f764510774710784910784a10794c117a4e117b4f127b51127c52137c54137d56147d57157e59157e5a167e5c167f5d177f5f187f601880621980641a80651a80671b80681c816a1c816b1d816d1d816e1e81701f81721f817320817521817621817822817922827b23827c23827e24828025828125818326818426818627818827818928818b29818c29818e2a81902a81912b81932b80942c80962c80982d80992d809b2e7f9c2e7f9e2f7fa02f7fa1307ea3307ea5317ea6317da8327daa337dab337cad347cae347bb0357bb2357bb3367ab5367ab73779b83779ba3878bc3978bd3977bf3a77c03a76c23b75c43c75c53c74c73d73c83e73ca3e72cc3f71cd4071cf4070d0416fd2426fd3436ed5446dd6456cd8456cd9466bdb476adc4869de4968df4a68e04c67e24d66e34e65e44f64e55064e75263e85362e95462ea5661eb5760ec5860ed5a5fee5b5eef5d5ef05f5ef1605df2625df2645cf3655cf4675cf4695cf56b5cf66c5cf66e5cf7705cf7725cf8745cf8765cf9785df9795df97b5dfa7d5efa7f5efa815ffb835ffb8560fb8761fc8961fc8a62fc8c63fc8e64fc9065fd9266fd9467fd9668fd9869fd9a6afd9b6bfe9d6cfe9f6dfea16efea36ffea571fea772fea973feaa74feac76feae77feb078feb27afeb47bfeb67cfeb77efeb97ffebb81febd82febf84fec185fec287fec488fec68afec88cfeca8dfecc8ffecd90fecf92fed194fed395fed597fed799fed89afdda9cfddc9efddea0fde0a1fde2a3fde3a5fde5a7fde7a9fde9aafdebacfcecaefceeb0fcf0b2fcf2b4fcf4b6fcf6b8fcf7b9fcf9bbfcfbbdfcfdbf";

// RdYlGn
specifier =
  "a50026d73027f46d43fdae61fee08bffffbfd9ef8ba6d96a66bd631a9850006837";

// From d3.js
var n = (specifier.length / 6) | 0,
  colors = new Array(n),
  i = 0;
while (i < n) colors[i] = "#" + specifier.slice(i * 6, ++i * 6);
colors.reduce(
  (str, col, index) =>
    `${str}<ColorMapEntry color='${col}' quantity='${(index * 255) /
      (colors.length - 1)}' opacity='1'/>\n`,
  ""
);
```

#### Or, with some function `col(t:[0,1]) => hexString:string`

```js
let n = 100;
new Array(n)
  .fill()
  .reduce(
    (str, nada, index) =>
      `${str}<ColorMapEntry color='${col(index / (n - 1))}' quantity='${(index *
        255) /
        (n - 1)}' opacity='1'/>\n`,
    ""
  );
```
