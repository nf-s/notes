# Julia

Supports Latex symbols  
Juliabox (Jupyter)

## OOP Similarities

No inheritance from concrete - only abstract types  
No multiple inheritance  
No encapsulation  
No class ownership of methods

## Functional Similarities

Not pure functional (pure functional = can't store values)

## Possible Issues

```julia
Pkg.add('Bio.jl')   #no
Pkg.add('Bio')      #yes
```

## Type Stability

```julia
sqrt(4)
# = 2.0

sqrt(Complex(-4)) # must specify complex argument
# = 0.0 + 2.0im
```

## Code Examples

+ Sin with degrees: `sind(30)`
+ String concat: `"Hello " * "world"`
+ Approx equal operator
+ `1 < x < 2`
+ `IntOrString = Union(Int,AbstractString)`
+ Subtype of Real: `<:Real`
+ `typeof(...)`
+ `A={i+10j for i=1:5, j=1:5};`
+ Array slicing: `A[1:2, :]`

**Struct**  
...

## Channels

*Any function which alters arguments - suffix with `!`*

... Multitasking

