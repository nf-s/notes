
# V8 JavaScript performance
https://www.youtube.com/watch?v=EhpmNyR2Za0

Array indexes (numerical properties) are storred seperately from object properties

Sparse (or holey) array with empty elements
```js
i.e. array.length; //5
array[8] = 4;
// Now indexes 5,6,7 are empty
```

Checking if a sparse array has an element is LONG
## Element Types
SMI (Small integer), Double, generic  
Then also Packed or Holey

Can only transition into less specific kinds (eg. when you add a double to a SMI array - it becomes a Double array)

ALSO can't transition from HOLEY to PACKED!  
Use always use `array.push()`  
Don't look for elements outside array bound

### To loop

```js
for (const item of items) {
    doSomething(item)
}
// OR
items.forEach(...)
```

Use typed arrays??
```js
logArgs = (...args) => {
	args.forEach(...)
}
```

### Side note
repl = read eval print loop