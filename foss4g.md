## State of Geoserver

S3 raster source

WMS get legend graphic JSON

Friday 12pm talk on styles

ogcapi

wfs3 talk 3:30

sldservice

channel selection band in env vars

geofence...

## GC2 / Vidi

...

## Geoserver rendering transformations

https://www.geo-solutions.it/blog/2017-05-00-release-mapstore/

SQL param. views...

WPS in SLD...

https://docs.geoserver.org/stable/en/user/community/remote-wps/index.html

## GeoNode

...

https://github.com/geosolutions-it/MapStore2

Kubernetes ccomign

## Raster processing...

https://www.mapzen.com/blog/mapping-mountains/

https://ivan.sanchezortega.es/

https://eox.at/2018/01/visualizing-geotiff-tiles-with-openlayers/

## Colormap bad...

## EOX Data vis

https://github.com/ungarj/mapchete

Look into plotty expressions (and clamp)

Graphly.js - https://github.com/EOX-A/graphly

Try stardust with Leaflet

## Carto WebGL vector

Re classify colours on zoom level

https://github.com/mapbox/mapbox-gl-js

https://github.com/CartoDB/carto-vl/tree/v1.4.1

Airship widget - tie filter to map

## pygeoapi

https://www.w3.org/TR/sdw-bp/

https://github.com/opengeospatial/OGC-Web-API-Guidelines

https://github.com/geopython/pygeoapi

https://climatedata.ca/explore/variable/?coords=62.51231793838694,-98.48144531250001,4&geo-select=&var=tx_max&var-group=temperature&mora=ann&rcp=rcp85&decade=1980s&sector=

http://habitatseven.com/

## Cog in the machine

200GB COG (single band)

https://www.addresscloud.com/

https://blog.mapbox.com/aws-lambda-python-magic-e0f6a407ffc6

https://blog.mapbox.com/build-for-the-cloud-with-rasterio-3254d5d60289

https://blog.mapbox.com/combining-the-power-of-aws-lambda-and-rasterio-8ffd3648c348

https://docs.addresscloud.com/#data-dictionary

## UNSORTED

Geomoose
Mapbender

https://www.earthobservations.org/activity.php?id=126

Geomaze
Geoweb

https://www.google.com/url?q=https://geoext.org/&sa=U&ved=2ahUKEwj-5oGOtKXkAhV8XhUIHTYPCh8QFjAAegQIAhAB&usg=AOvVaw1XorgfmPsb54Q6HD01_h9V

https://parallel.co.uk/netherlands/#11.48/52.3801/4.876/130.1/60

https://uclab.fh-potsdam.de/vff/

Tensorflow raster processing...

https://github.com/mapbox/mapbox-gl-js/issues/1489

https://github.com/ladislavhorky/battle-of-3d-rendering-stacks

Keplr.gl - Uber mapping built on top of mapbox gl

Maputnik - map styler

https://www.safe.com/fme/new-features/2019/

Foreign Data Wrappers FDW you can access remote objects from SQL database

https://www.osgeo.org/projects/orfeo-toolbox/

Postgis cog,,,

istSOS

Oskari

https://climate-adapt.eea.europa.eu/knowledge/adaptation-information/climate-services

https://cds.climate.copernicus.eu/cdsapp#!/dataset/projections-cmip5-monthly-single-levels?tab=form

https://cds.climate.copernicus.eu/cdsapp#!/dataset/projections-cmip5-daily-single-levels?tab=form - has CSIRO projections
