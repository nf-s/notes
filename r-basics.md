# Refresher

https://learnxinyminutes.com/docs/r/

## For loops

```R
for (i in 1:4) {
  print(i)
}
```


# Utility functions

## Working Dir

### Set

```R
setwd(x)
```

### Get

```R
getwd()
```

## To PDF


```R
1. pdf(file, height, width)
2. dev.off()
```

## Save/Load R workspace
```R
save(…, file)
```
+ … – List all objects that should be saved.
+ file – Provide the file name, typically ending in .rda. Also, because the list of objects to save is arbitrarily long, the file argument must begin with the argument name, i.e. include file= in this argument’s declaration.

```R
save.image(file)
```
+ file – Provide the file name, typically ending in .rda or .RData.

```R
load(file)
```
+ file – The name of the file to be loaded.


## Install Packages


```R
install.packages("package",repos = "https://mirror.aarnet.edu.au/pub/CRAN/", dependencies = TRUE)
```

## Help
+ show docs - `?function`
+ search docs - `??function`

---

# Object functions

## Vector ("collection")

```R
c(x1, x2, ...)
```

### Double-Precision Vectors

Creates empty column vector of size n


```R
double(n)
```

### Generate regular sequence


```R
seq(from, to, by)
```

### Repeat


```R
rep(v1, times, "each")
```

### Concat vectors


```R
paste(v1, v2..., sep="")
```

### Combine R objects
"Or row bind"  


```R
rbind(x1, x2...)
```

### Sort object


```R
sort(obj,decreasing=TRUE)
```

### Matrix


```R
matrix(x, nrow, ncol, byrow, dim.names)
```

+ `byrow` = "row" or "col"

#### Transpose


```R
t(matrix)
```

# Data Frame
- matrix = rows+cols  
- data.frame=variable(sx) + observations  


```R
data.frame(d)
```

To set column names with vectors


```R
data.frame(col1=v1, col2=v2...)
```

Get dataframe column:


```R
df$column
```

Set column names:


```R
colnames(df)<-c("col1","col2"...)
```

Set row names:


```R
rownames(df)<-c("row1","row2"...)
```

Get dataframe shape/dimensions


```R
dim(df)
```

Get number of rows


```R
nrow(df)
# Or
length(obj)
```

#### Subsets/Slicing:

+ `d[5,]` - only row 5
+ `d[-5,]` - without row 5  
+ `d[,c(1,3)]` - cols 1 and 3 
+ `d[d$col=="x",]` - rows where column "col" ==  "x"

#### Filter by values

Use a vector to select rows in dataframe  
4 methods:


```R
df <- data.frame(id=..., x=...)
L <- c("A","B","E")
```

Method 1:


```R
df[id %in% L,]
```

Method 2:


```R
subset(df, id %in% L)
```

Method 3:
If ids are unique


```R
df <- data.frame(id=c(LETTERS), x=1:26)
df[match(L, df$id), ]
```

#### Adding rows/cols

+ `cbind()` - Column bind
+ `rbind()` - Row Bind

### Order/Sort dataframe


```R
df[order(df$X1, decreasing = TRUE),]
```

### Remove samples (rows) from dataframe


```R
rows.to.remove <- c(1, 2, 3...)
df[-rows.to.remove,]
```

#### Sample values from dataframe


```R
sample(df, n, "replace"=TRUE)
```

+ `"replace"=TRUE` allows for duplicate samples

#### Sample rows from dataframe:

```R
df[sample(nrow(df), n), ]
```

#### Only use rows with *NO* missing data


```R
complete.cases(df)  
na.omit(df)
df[!is.na(df), ]
```

#### Add list to data.frame (nested list)

<https://www.r-bloggers.com/populating-data-frame-cells-with-more-than-one-value/>

```R
d[i , "col_name"][[1]] <- list(c("A", "B", "C"))
```

#### Create a Data Frame from All Combinations of Factor Variables


```R
expand.grid(X1=f1, X2=f2,...)
```

#### Apply function to dataframe column by column
Also apply over list or vector
<http://www.datasciencemadesimple.com/apply-function-r/>


```R
A <- function(x) x + 1

data.frame(df, lapply(df, A) )
#or
cbind(df, lapply(df, A) )
```

#### Apply function row by row


```R
rapply is a recursive version of lapply.
```

lapply returns a list  
sapply returns a vector !!!!!!!

#### "Join" two dataframes by value - Add values from one dataframe based on another value


```R
df.addTo$newCol <- merge(df.addFrom[, c("colToAddToDf", "mutualCol")], df.addTo, by="mutualCol")

df.addTo$newColt<-with(df.addFrom, colToAddToDf[match(df.addTo$mutualCol, mutualCol)])
```

+ all=TRUE
   + Will keep all rows (not just mutual rows)

### Convert boolean vector (Flase/True) into index (or which indices are TRUE)
Creat new vector based on conditions

If subset.of.df and list.example are the same size
+ will return "corresponding" rows of list.example


```R
list.example[which(df %in% subset.of.df)]
```

### Find maxima/minima in dataframe
Returns index


```R
which.max(vector)
```

## Logical Vectors

#### Operations
+ x & y	Returns the result of x and y
+ x | y	Returns the result of x or y
+ ! x	Returns not x
+ xor( x, y )	Returns the result of x xor y (x or y but not x and y)


```R

```

# Lists
https://www.r-bloggers.com/how-to-use-lists-in-r/
### Apply a Function over a List or Vector


```R
lapply(X, FUN, ...)
```

### Flatten Lists
Simplifies to produce vector


```R
unlist(x, recursive = TRUE, use.names = TRUE)
```

# Factors
### Turn a vector into a factor. 
Can set the levels of the factor and the order.


```R
factor(x)
as.factor(x)
```

### Turn a numeric vector into a factor but ‘cutting’ into sections. 


```R
cut(x, breaks = 4)
```

### Unfactorise


```R
as.character(fixed$Type)
```

### Factor (which is number) to actual numerical value (not factor level)
Remember - factors are represented with integers - 0, 1, ..., n


```R
# this is recommended 
as.numeric(levels(f))[f]
# and slightly more efficient than code below
as.numeric(as.character(f))
```

### Reverse row order of dataframe


```R
df[rev(rownames(df)),]
```

## String functions
https://www.statmethods.net/management/functions.html
https://www.r-bloggers.com/string-functions-in-r/

### Split string


```R
strsplit(as.character(my.data[1,1]), "/")
```

### Length of String (number of characters)


```R
nchar(str)
```

### Pad string with zeros


```R
library(stringr)
str_pad(str, nchar, pad = "0")
```

## Object Utility Functions


```R
is.data.frame(d)    
is.array   
is.vector...

typeof(d)  
class(d)
```

#### Get object structure


```R
str(obj)
```

#### Get object subvariables


```R
names(obj)
```

### Remove objects from workspace


```R
rm(...)
```

# Pattern Matching (grep)

### Grep


```R
grep(pattern, x, ignore.case = FALSE, perl = FALSE, value = FALSE,
     fixed = FALSE, useBytes = FALSE, invert = FALSE)

grep (value=FALSE) # returns indices vector

grep (value=TRUE) # returns vector of found values

# returns logical vector
grepl(pattern, x, ignore.case = FALSE, perl = FALSE,
      fixed = FALSE, useBytes = FALSE)

sub(pattern, replacement, x, ignore.case = FALSE, perl = FALSE,
    fixed = FALSE, useBytes = FALSE)
```

#### Filter Dataframe


```R
df[grepl('*pattern*', df$col),]
```

---

# Read/Write data from/to files
### Read table


```R
d.f <- read.table("file",header=TRUE)
```

Params (=DEFAULT):
+ `header=FALSE`

#### Fix warning 'EOF within quoted string'
+ Add `quote = ""`


```R
convicts <- read.table("Convict Arrivals v201605.txt",header=TRUE, sep = '\t', fill = TRUE, quote = "")
```

### Write table


```R
write.table(df,"path",sep="\t", row.names=FALSE)
```

#### To clipboard:


```R
write.table(df,"clipboard",sep="\t")
```

Params (=DEFAULT):
+ `row.names=TRUE`

## JSON
https://cran.r-project.org/web/packages/jsonlite/vignettes/json-aaquickstart.html


```R
library(jsonlite)

all.equal(mtcars, fromJSON(toJSON(mtcars))) # returns true

# Save to file
write(toJSON(sicklist.final1), "sicklist.final1.json")
```

---

# Statistical functions
### Statistics
+ Standard deviation - `sd(vector)`
+ Varaince - `var(v)`
+ Covariance - `cov(x, y)`
+ Correlation - `cor(x, y)`
+ `mean(), median()...`

### Poison


```R
pois(x, lambda)
```

## Contingency table (categorical vars table)
https://stat.ethz.ch/R-manual/R-devel/library/stats/html/xtabs.html


```R
xtabs(Y~X,data)
```

Example:  


```R
xtabs(~Treat + Poison, data = dd)
```

## Compute Summary Statistics of Data Subsets


```R
aggregate(formula~,data,func)
```

Example:  


```R
aggregate(weight ~ feed, data = chickwts, mean)
```

Formulas: 
+ one ~ one
+ one ~ many
+ many ~ one
+ many ~ many

** Note - can combine with xtabs **

## Apply a function to each cell of a ragged array.
That is to each (non-empty) group of values given by a unique combination of the levels of certain factors


```R
tapply(d$Y, d$Factor, func)
```

+ `func` = mean, median...

----------
# Files

### List files
https://stat.ethz.ch/R-manual/R-devel/library/base/html/list.files.html


```R
list.files(path = ".", pattern = NULL, all.files = FALSE,
           full.names = FALSE, recursive = FALSE,
           ignore.case = FALSE, include.dirs = FALSE, no.. = FALSE)

       dir(path = ".", pattern = NULL, all.files = FALSE,
           full.names = FALSE, recursive = FALSE,
           ignore.case = FALSE, include.dirs = FALSE, no.. = FALSE)

list.dirs(path = ".", full.names = TRUE, recursive = TRUE)
```

https://stat.ethz.ch/R-manual/R-devel/library/base/html/files.html


```R
file.create(..., showWarnings = TRUE)
file.exists(...)
file.remove(...)
file.rename(from, to)
file.append(file1, file2)
file.copy(from, to, overwrite = recursive, recursive = FALSE,
          copy.mode = TRUE, copy.date = FALSE)
file.symlink(from, to)
file.link(from, to)
```

# Dates
https://www.stat.berkeley.edu/~s133/dates.html

### String to Date


```R
install.packages('chron')
library('chron')

chron(dates=unlist(df$date),times=unlist(df$time),format=c('ymd','hms'))
```

### Format Date


```R
format(possible.samples$datetime, '%H%M%S')
```

### From Numerical Variables


```R
ISOdate(year, month, day)
```
