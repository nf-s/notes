# CSS Grid Terminology
+ container
+ item
+ line
+ cell
+ track
+ area
+ gap

## Grid container
`display: grid;`

## Grid item
direct decendant of the grid container

## Grid line
Referenced by number, starting with outer borders of the grid

## Grid cell

## Grid area
Rectangle of cells

## Grid track
Space between two or more adjacent grid lines
+ Row tracks, Column tracks...

## Grid gap
Space between cells (gutters)

# Example
1. `display: grid;`
2. `grid-template-columns: 2fr 1fr 1fr;`
   + fr = fraction
3. `grid-template-rows: auto 1fr 3fr;`
4. Grid items automiatically populate cells

## How to define start and end grid lines for element
+ From column line 2 to 4
    + `grid-column: 2/4`
+ From row lines 2 to 3
    + `grid-row: 2/3`

# Grid template areas
Use text-based grid
```css
grid {
grid-template-areas:
    "title title title"
    "main header header"
    "main sidebar footer";
}

element {
    grid-area: header;
}
```

## Backward Compatability
```css
@supports (display:grid) {
    ...
}
```

If don't support grid - sever mobile layout

Build accessible mobile-first layout without grid - **Use as fallback**

At appropriate breakpoint - add grid

https://gridbyexample.com/

https://developer.mozilla.org/en-US/docs/Web/CSS/grid