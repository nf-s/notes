Based on http://rajapradhan.com/blogs/d3-js-v4-essentials

# D3-selections
```javascript
D3.select(selector) #selects first element  
D3.selectAll(selector)
```

Can then use .attr() .style()…  
.node()/.nodes() to select Dom elements  

### i.e.
```javascript
D3.selectAll('p').nodes()[0];
```

## Modifying Elements
```javascript
selection.style(name[, value[, priority]])
```

Priority = null or "important" ( no !)

Can be a constant or a function (called accessor functions) with three parameters:
+ Current datum (d) - data bound to current element
+ Current index (i)
+ Current group (nodes) - dom reference array of selection

### Eg - setting p elements to red
```javascript
d3.selectAll("p")
  .style("color", "red");
```

**or**
```javascript
d3.selectAll("p")
  .style("color", function(d, i) {
    return "red";
});
```

## Modifying attributes
Same deal

### Eg 
```javascript
var svgRef = d3.select("svg")
            .attr("width", 500)
            .attr("height", 100);
```
 
```javascript
svgRef.attr("width"); // "500"
```

Other Modifications
+ Properties - selection.property(name[, value])
+ Classes - selection.classed(names[, value])

Example set class
```javascript
d3.selectAll("p").classed("foo bar", true);
```

**OR**
```js
.select(".domain").remove();
```

## Appending an Element
```javascript
selection.append(tag name)

# Example
d3.selectAll("div")
  .append("p");
```
This method returns selection containing appended elements

## Inserting an element
```javascript	
selection.insert(type[, before])
```
Where `before` = before element

## Removing an element
```javascript	
selection.remove()
```

## Filtering elements from selection
```javascript	
selection.filter(filter)
```
`filter` Can be a selector string or function  
+ Eg - `:nth-child(even)`

## Merging selections
```javascript	
selection.merge(other)
```
This method is commonly used to merge the enter and update selections after a data-join.

## The each() method
Invokes function for each of the selected elements
```javascript	
d3.selectAll("div").each(function(d, i) {
    d3.select(this)
      .selectAll("p")
        .style("color", "blue");
});
```

-----------

# Data Formats and Data Binding
Formats:
+ Data array (d3-array)
+ Collection - Object, Map, Set... (d3-collection)
+ Creating nested data (d3-collection)
+ Loading external files (d3-dsv)
+ Data Binding

## Array
### Common Methods

+ `d3.min(array[, accessor])` : Returns the minimum value in the given array using natural order. If the array is empty, returns undefined.
+ `d3.max(array[, accessor])` : Returns the maximum value in the given array using natural order. If the array is empty, returns undefined.
+ `d3.extent(array[, accessor])` : Returns the minimum and maximum value in the given array using natural order. If the array is empty, returns [undefined, undefined].
+ `d3.sum(array[, accessor])` : Returns the sum of the given array of numbers. If the array is empty, returns 0. This method ignores undefined and NaN values.
+ `d3.mean(array[, accessor])` : Returns the mean of the given array of numbers. If the array is empty, returns undefined.
+ `d3.merge(arrays`) : Merges the specified arrays into a single array.
+ `d3.ticks(start, stop, count)` : Returns an array of approximately count + 1 uniformly-spaced, nicely-rounded values between start and stop (inclusive).
+ `d3.tickStep(start, stop, count)` : Returns the difference between adjacent tick values.

### Eg
```js
d3.max([
         {name: 'John', age: 28}, 
         {name: 'Jack', age: 35}
       ], function(d, i) {return d.age;}); // 35
```
```js
// Note the following
d3.min(["52", "8"]); // "52" - This is because elements are compared using natural order rather than numeric order.
d3.max(["12", "2"]); // "2" - This is because elements are compared using natural order rather than numeric order.
```
 
## Collection
### Common Methods
+ `d3.keys(object)` : Returns an array containing the property names of the specified object. The order of the returned array is undefined.
+ `d3.values(object)` : Returns an array containing the property values of the specified object. The order of the returned array is undefined.
+ `d3.entries(object)` : Returns an array containing the property keys and values of the specified object. Each entry is an object with a key and value attribute.

### Creating nested data - example
```js
var tweets = [
	{user: 'John', topic: 'technology', numberOfTweets: 10},
	{user: 'John', topic: 'politics', numberOfTweets: 30},
	{user: 'Jack', topic: 'technology', numberOfTweets: 20},
	{user: 'John', topic: 'entertainment', numberOfTweets: 5},
	{user: 'Jack', topic: 'politics', numberOfTweets: 10}
];
```

```js
d3.nest()
  .key(function(d) { return d.user; })
  .entries(tweets);

// Output:
[
  {
    key: "John",
    values: [
      {numberOfTweets: 10, topic: "technology", user: "John"},
      {numberOfTweets: 30, topic: "politics", user: "John"},
      {numberOfTweets: 5, topic: "entertainment", user: "John"}
    ]
  },
	
  {
    key: "Jack",
    values: [
      {numberOfTweets: 20, topic: "technology", user: "Jack"},
      {numberOfTweets: 10, topic: "politics", user: "Jack"}
    ]
  }
]
```

## Loading data from external files
+ d3.csvParse("Some,string")
+ d3.tsvParse("Some\tstring")
+ d3.csvFormat(SomeObject)
+ d3.tsvFormat(SomeObject)

### JSON - use Axios
```js
axios.get('/data/sicklist.final1.json')
  .then((response) => {
    console.log(response);
  })
  .catch((error) => {
    console.log(error);
  });
```

### Custom delimiter
```js
var psv = d3.dsvFormat("|");
psv.parse("name|age\nJohn|28"); 
```

## Data Binding
```js
var circles = d3.select("svg")
  .selectAll("circle")
  .data([30, 40]);
```

-----

# Enter-Update-Exit Pattern
**Enter set**  
DOM Elements which need to be added for corresponding data points
```js
circles.data(data).enter();
```


**Exit set**  
DOM Elements which need to be removed - when they have no corresponding data points
```js
circles.data(data).exit();
```

**Update set**  
DOM Elements which have data binding and may need updatating if corresponding data values change  
```js
circles.data(data);
```

### Example
```js
// Select all existing circles and bind data to it
var circles = d3.selectAll("circle")
  .data(data);
 
// Update existing circles
circles.attr("fill", "red");
 
// Remove surplus circles
circles.exit().remove();
 
// Add new circles and then create a new selection
// merging the new and the existing circles and then
// update the merged selection
circles.enter()
  .append("circle")
  .merge(circles)
  .attr("fill", "green");
```

# Scales
>Scales are functions that map from an input domain to an output range.   
*Mike Bostock*

Types:

+ Continuous (Linear, Power, Log, Identity, Time)
+ Sequential
+ Quantize
+ Quantile
+ Threshold
+ Ordinal (Band, Point, Category)

### Linear Scale Eg
```js
var yScale = d3.scaleLinear()
    .domain([10, 1200]) // min and max value in our data array
    .range([0, 500]); // min and max value of our svg container
 
// Now we can see how our values are mapped
yScale(10); // 0
yScale(500); // 205.88
yScale(980); // 407.56
yScale(1200); // 500
```

### Colour Scale Eg
```js
var colorScale = d3.scaleLinear()
  .domain([1, 10])
  .range(["red", "green"]);
colorScale(1); // "rgb(255, 0, 0)"
colorScale(10); // "rgb(0, 128, 0)"
```

## Interpolators
...

## Clamping
Prevents extrapolation (i.e. returning value outside range)
```js
yScale.clamp(true);
```

-----

# Axes
Note `<g>` is a svg group element
## Common Methods

+ `d3.axisTop(scale)` : A horizontal axis with labels placed on top of the axis
+ `d3.axisBottom(scale)` : A horizontal axis with labels placed at the bottom of the axis
+ `d3.axisLeft(scale)` : A vertical axis with labels placed on the left hand side of the axis
+ `d3.axisRight(scale)` : A vertical axis with labels placed on the right hand side of the axis

## Example
```js
var svgContainer = d3.select("body")
        .append("svg")
          .attr("width", 500)
          .attr("height", 400)
          .style("padding", "10px")
          .style("border", "1px solid");
      
      var xScale = d3.scaleLinear()
          .domain([0, 100])
          .range([0, 500]);
      
      var xAxis = d3.axisBottom(xScale);
      
      svgContainer.append("g")
          .call(xAxis);
      
      // To place the axis at the bottom of the container
      /*
      svgContainer.append("g")
          .attr("transform", function() {
            return "translate(" + 0 + "," + 390 + ")"
          })
          .call(xAxis);
      */
```

### Style Example (Remove axis line)
```js
.call(xAxis)
    .select(".domain").remove();
```

## Customising Ticks
```js
var xAxis = d3.axisBottom(xScale)
    .ticks(5)
    .tickPadding(10)
    .tickFormat(function(d) { return d + "%"; })
```



## Drawing grid lines
Examplke from Data Visualization with D3.js Cookbook (Drawing Grid Lines – Chapter 5).
```js
// First declare some variables for dimension and margin for the container
  var width = 500;
  var height = 500;
  var margin = 25;
  var axisLength = width - 2 * margin; // Leave margin on both sides left-right or top-bottom
  
  // The main container to house our axes groups
  var svgContainer = d3.select("body")
	.append("svg")
	  .attr("width", width)
	  .attr("height", height)
	  .style("border", "1px solid");
 
  // Renders the X-axis and the vertical grid lines
  function renderXAxis() {
	var xScale = d3.scaleLinear()
	  .domain([0, 100])
	  .range([0, axisLength]);
	
	var xAxis = d3.axisBottom(xScale);
	
	svgContainer.append("g")
        // Give a class name to the x-axis group so that we can target it
		.classed("x-axis", true)
		.attr("transform", function() {
          // Translate the x-axis to the bottom of the container leaving
          // margin on the left and bottom
		  return "translate(" + margin + "," + (height - margin) + ")";
		})
		.call(xAxis);
	
        // For each of the tick component create a line element inside the group
	    // This creates the vertical lines of the grid
        svgContainer.selectAll("g.x-axis g.tick")
	    .append("line")
		.classed("grid-line", true)
         // x1,y1 sets the starting point of the line
         // x2,y2 sets the destination point of the line
         // Since the line is inside the g element and the g element is already
         // translated we just need to set the y2 value
		.attr("x1", 0)
		.attr("y1", 0)
		.attr("x2", 0)
		.attr("y2", -(height - 2 * margin));
  }
  
  // This creates the horizontal lines of the grid
  function renderYAxis() {
	var yScale = d3.scaleLinear()
	  .domain([100, 0])
	  .range([0, axisLength]);
	
	var yAxis = d3.axisLeft(yScale);
	
	svgContainer.append("g")
		.classed("y-axis", true)
		.attr("transform", function() {
		  return "translate(" + margin + "," + margin + ")";
		})
		.call(yAxis);
	
	svgContainer.selectAll("g.y-axis g.tick")
	  .append("line")
		.classed("grid-line", true)
		.attr("x1", 0)
		.attr("y1", 0)
		.attr("x2", axisLength)
		.attr("y2", 0);
  }
  
  renderXAxis();
  renderYAxis();
```

# Transitions
all in ms

```js
selection.transition()
        .ease(ease)
        .delay(delay)
        .duration(2000)
        .style("transform", "translateX(400px)")
        .style("background-color", "blue");
      
```

Ease:
+ `d3.easeElastic`
+ ...

## Chained Transitions
```js
selection.transition().
        .delay()
        ...
    .transition()
        ...
```

## Transition Event Handing
`transition.on(eventType, [listener])`

+ start
+ end
+ interrup

```js
.transition()
...
.on("start", function() {           console.log("Started"); })
    
.on("end", function() {             console.log("Finished"); })
```

# Shapes
d3 shape generators

## Line generator
Can be used to create virtually any kind of shape

Uses `svg:path` element **not** `svg:line`
It basically generated the `<d>` attribute's value

### Eg
```js
var data = [
	{x: 0, y: 4},
	{x: 1, y: 9},
	{x: 2, y: 6},
	{x: 4, y: 5},
	{x: 6, y: 7},
	{x: 7, y: 3},
	{x: 9, y: 2}
];

var line = d3.line()
    .x(function(d) { return xScale(d.x); })
    .y(function(d) { return yScale(d.y); });

svgContainer.append("path")
  .attr("d", line(data))
  .attr("fill", "none")
  .attr("stroke", "red")
```

## Line interpolators (curves)
Default is `d3.curveLinear`  
Others:
+ d3.curveBasis
+ d3.curveStep
+ d3.curveCardinal (looks nice)

## Area generator
Only difference - sepcify lower (x0, y0) and upper (x1, y1) bounds

```js
var area = d3.area()
    .x(function(d) { return xScale(d.x); })
    .y0(yScale(0))
    .y1(function(d) { return yScale(d.y); })
```

## Area interpolators
Same as line interpolators

## Arc generator
circle, annulus (donut), sectors...

+ `innerRadius(r)` - if r>0 => create annulus
+ `outerRadius(r)`
+ `startAngle(angle)` - 
+ `endAngle(angle)`

**Note**  
The angle is in radians with 0 at –y (12 o’clock) and positive angles proceeding clockwise.

## Arc transitions
Cannot use normal d3 transitions (only style)

Must use `attrTween()`

```js
arcs.forEach(function(d, i) {
    group.append("path")
      .attr("fill", color(i))
      .transition()
      .duration(2000)
      .attrTween("d", function() {
         var start = {startAngle: 0, endAngle: 0};
	 var interpolate = d3.interpolate(start, d);
	     return function(t) {
	       return arc(interpolate(t));
	     };
      })
});
```

---
# Layouts
Take data and convert to a more convenient format (for use in generators or drawing SVG/canvas)

E.g. d3.pie()  
Others:
+ Pie
+ Stack
+ Tree
+ Treemap
+ Pack
+ Force
+ Histogram
+ Cluster

# Interactions
Mouse events, zoom/pan, drag...

```js
d3.select("svg")
    .on("mouseover", function() { // register a listener for the mouseover event
        printPosition();
    });
 
d3.select("text")
    .text(d3.mouse(svg.node())); // Update the label with the current coordinates of the mouse
```

*The d3.mouse(container) method returns the current (x,y) position of the mouse relative to the specified container.*

## Zoom and pan (d3-zoom)
```js
function zoomed() {
  svgG.attr("transform", "translate(" + d3.event.transform.x + "," + d3.event.transform.y + ") scale(" + d3.event.transform.k + ")");
}

var zoom = d3.zoom()
    .scaleExtent([1, 10])
    .on("zoom", zoomed);

var svgG = d3.select("body")
  .append("svg")
    .attr("width", width)
    .attr("height", height)
    .style("border", "1px solid")
    .call(zoom) // This is how we attach zoom behavior to the container
  .append("g")
```

## Drag (d3-drag)
```js
d3.drag()
  .on("start", started) // after a new pointer becomes active (on mousedown or touchstart).
  .on("drag", dragged) // after an active pointer moves (on mousemove or touchmove).
  .on("end", ended); // after an active pointer becomes inactive (on mouseup, touchend or touchcancel).

  d3.select(".draggable")
  .call(drag);

function dragged() {
  var x = d3.event.x; // Get current x position
  var y = d3.event.y; // Get current y position
  
  d3.select(this)
      // Translate the selection to the current x and y position
      .attr("transform", "translate(" + x + "," + y + ")");
}
```


# Chart Examples
## Barchart
```js
var data = [10, 15, 25, 120, 500, 980, 1200];
      
      // Create yScale
      var yScale = d3.scaleLinear()
                     .domain([10, 1200])
                     .range([0, 500]);
      
      d3.select("svg")
        .selectAll("rect")
        .data(data)
        .enter()
        .append("rect")
          .attr("width", 30)
          .attr("height", function(d) { return yScale(d); })
          .attr("x", function(d, i) { return i * 30; })
          .attr("y", function(d) { return 500 - yScale(d);})
          .style("fill", "blue")
		  .style("stroke", "black")
		  .style("stroke-width", "1px")
		  .style("opacity", .25);
```