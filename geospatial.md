# Geospatial ...

## Raster data

### Reproject

```bash
gdalwarp -t_srs EPSG:3857 -of GTiff -co NUM_THREADS=ALL_CPUS -co COMPRESS=DEFLATE -co PREDICTOR=2 -co ZLEVEL=9 -co TILED=YES inFilename outFilename
```

## Merge bands

```bash
gdal_merge.py -separate -o new_rgb.tif -co PHOTOMETRIC=MINISBLACK r.tiff g.tiff b.tiff
```

## Vector data problems

### Polygon with self intersection

https://gis.stackexchange.com/questions/265661/updated-fix-for-polygon-self-intersection-qgis-2-18-14/265667

Run `buffer` in QGIS with distance set to 0

## Ogr2ogr

### GeoJSON to shapefile

From https://gis.stackexchange.com/a/73026

```bash
ogr2ogr -nlt POINT -skipfailures points.shp geojsonfile.json OGRGeoJSON
ogr2ogr -nlt LINESTRING -skipfailures linestrings.shp geojsonfile.json OGRGeoJSON
ogr2ogr -nlt POLYGON -skipfailures polygons.shp geojsonfile.json OGRGeoJSON
```

### Shapefile to GeoJSON

```bash
ogr2ogr -f GeoJSON your_data_in_4326.json -t_srs EPSG:4326 your_data.shp
```

## GDB to geojson

```bash
ogr2ogr -f GeoJSON Recycled_Water_Offsets.json ./GQCCCRA_Study_Area.gdb -t_srs EPSG:4326 Recycled_Water_Offsets


```

#### Drop attributes

```bash
ogr2ogr -f GeoJSON -sql "SELECT fid FROM input" your_data_in_4326.json -t_srs EPSG:4326 your_data.shp
```

## Mapbox tiles

### Shapefile/GeoJSON to standalone tiles

From https://github.com/klokantech/vector-tiles-sample#host-the-vector-tiles-without-any-server-at-all

```bash
ogr2ogr -f GeoJSON your_data_in_4326.json -t_srs EPSG:4326 your_data.shp
tippecanoe -o out.mbtiles -zg --drop-densest-as-needed your_data_in_4326.json
mb-util out.mbtiles data-layer-name --image_format=pbf
gzip -d -r -S .pbf *
find ./data-layer-name -type f -exec mv '{}' '{}'.pbf \;
```

```bash
tippecanoe -zg -o ne_10m_populated_places.mbtiles -r1 --cluster-distance=10 --accumulate-attribute=POP_MAX:sum ne_10m_populated_places.geojson
```

```bash
tippecanoe -aC -o land-parcels-ac.mbtiles -r1 -zg ne_10m_populated_places.geojson
```

#### Other options:

--minimum-zoom=10
